import { createContext } from "react";

const DeckContext = createContext(null);

export default DeckContext;