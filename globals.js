export const Colors = {
  salmon: '#De4c4c',
  sky: '#328ace',
  lucky: '#20b45e',
  banana: '#e2d000',
  sunset: '#f2810f'
}