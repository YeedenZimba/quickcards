import React, { useState, useEffect, useContext, useRef } from 'react';
import { View, TouchableOpacity, FlatList, RefreshControl, StyleSheet } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { Searchbar, IconButton, RadioButton, Button, Chip, Text, Appbar } from 'react-native-paper';
import * as ScreenOrientation from 'expo-screen-orientation';

import StatusSpace from '../components/statusSpace';

import { Colors } from '../globals';

const iconSize = 30;

const ViewCard = ({route, navigation }) => {
  const { name, theme, cids } = route.params;
  let { index, cardsViewInfo, ...editParams } = route.params;
  editParams = {...editParams, cid: cardsViewInfo[index].cid, title: cardsViewInfo[index].title, note: cardsViewInfo[index].note, order: cardsViewInfo[index].order}
  const [isLandscape, setLandscape] = useState(false);

  useEffect(() => {
    const unsub = navigation.addListener('beforeRemove', (e) => {
      ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT)
      .then(() => {
        //pass
      })
      .catch(err => {
        e.preventDefault();
      })
    })

    return unsub;
  }, []);

  function toggleOrientation() {
    const newIsLandscape = !isLandscape;
    if (newIsLandscape){
      ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE)
      .then(() => {
        setLandscape(true);
      })
    } else{
      ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT)
      .then(() => {
        setLandscape(false);
      })    
    }
  }

  return (
    <View style={qcs.container}>
      <StatusSpace/>
      <View style={[qcs.w100, qcs.px2, isLandscape ? qcs.flexRow : qcs.flexColumn, qcs.flex, qcs.mb2]}>
        <View style={[isLandscape ? qcs.flexColumnReverse : qcs.flexRow, qcs.jcBetween]}>
          <IconButton
            icon="phone-rotate-landscape"
            size={iconSize}
            onPress={toggleOrientation}
            iconColor={Colors[theme]}
          />
          <IconButton
            icon="pencil-outline"
            size={iconSize}
            iconColor={Colors[theme]}
            onPress={() => {
              ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT)
              .then(() => {
                navigation.navigate("EditCard", editParams);
              })
            }}
          />
          <IconButton
            icon="close"
            size={iconSize}
            onPress={() => {
              navigation.goBack();
            }}
            iconColor={Colors[theme]}
          />
        </View>
        <View style={[qcs.flex, qcs.center]}>
          <View style={[isLandscape ? styles.cardContainterLandscape : styles.cardContainerPotrait]}>
            <Text variant='titleMedium' style={[{color: Colors[theme]}, qcs.b]}>{name}</Text>
            <View style={[isLandscape ? styles.cardLandscape : styles.cardPotrait, qcs.p1, {backgroundColor: theme === 'default' ? 'white' : Colors[theme]}]}>
              <Text variant='titleMedium' style={[qcs.textCenter, qcs[theme], qcs.b, qcs.u]}>{cardsViewInfo[index].title}</Text>
              <View style={[qcs.flex, qcs.vCenter]}>
                <View style={[qcs.flexRow, qcs.jcBetween, qcs.aiCenter]}>
                  <IconButton
                    icon="menu-left"
                    size={iconSize}
                    iconColor={theme === "default" ? 'black' : 'white'}
                    disabled={index === 0}
                    onPress={() => {
                      navigation.setParams({...route.params, index: index - 1})
                    }}
                  />
                  <Text variant='titleMedium' style={[qcs.textCenter, qcs[theme]]}>
                    {cardsViewInfo[index].note}
                  </Text>
                  <IconButton
                    icon="menu-right"
                    size={iconSize}
                    iconColor={theme === "default" ? 'black' : 'white'}
                    disabled={index === cardsViewInfo.length - 1}
                    onPress={() => {
                      navigation.setParams({...route.params, index: index + 1})
                    }}
                  />
                </View>

              </View>
              <Text variant='titleMedium' style={[qcs.textCenter, qcs[theme], qcs.b]}>{cardsViewInfo[index].order}/{cids.length}</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  )
}


const styles = StyleSheet.create({
  cardContainerPotrait: {
    width: '100%',
    maxWidth: 400
  },
  cardContainterLandscape: {
    width: '100%',
    height: '100%',
    maxWidth: 500,
    justifyContent: 'center'
  },
  cardPotrait: {
    height: 300,
    borderRadius: 10
  },
  cardLandscape: {
    flex: 1,
    maxWidth: 500,
    maxHeight: 300,
    borderRadius: 10
  }
});


export default ViewCard;