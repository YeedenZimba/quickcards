import React, { useState, useEffect, useContext, useRef } from 'react';
import { View, TouchableOpacity, FlatList, RefreshControl } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { Searchbar, Appbar, Button, Chip, Text, IconButton } from 'react-native-paper';
import Icon from 'react-native-paper/src/components/Icon';

import Card from '../components/card';

import StateContext from '../contexts/state';
import DeckContext from '../contexts/deck';
import CardContext from '../contexts/card';

import { Colors } from '../globals';

const Cards = ({ route, navigation }) => {
  const { did, cids, theme, name } = route.params;
  const [ isSearching, setSearching ] = useState(false);
  const [ refreshing, setRefreshing ] = useState(false);
  const [search, setSearch] = useState("");

  const { state, setAuth, setState, setDialogBox } = useContext(StateContext);
  const { cards, setCards, cardSortType } = useContext(CardContext);
  const { decks, setDecks } = useContext(DeckContext);
  
  const deckCards = Object.values(cards).filter(card => cids.includes(card.cid));
  const [ currentCards, setCurrentCards ] = useState(deckCards);
  const cardsViewInfo = currentCards.map(card => {return {cid: card.cid, title: card.title, note: card.note, order: card.order}})

  useEffect(() => {
    let ignore = false;

    const fetchData = async () => {
      setState("loading");
      try{
        const cardsRes = await fetch(`https://quickcards-api.vercel.app/cards/${did}`, {
          method: "GET",
          credentials: "include"
        });
        if (cardsRes.status === 403) return setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);; 
        const jsonCards = await cardsRes.json();
        const cardDict = {};
        jsonCards.forEach(d => {
          d.doc = new Date(d.doc);
          d.order = cids.indexOf(d.cid) + 1;

          cardDict[d.cid] = d;
        });

        if (ignore) return;

        setCards({...cards, ...cardDict}); 
        setState(null);   
      }
      catch (err){
        setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
      }

    }

    for(let i = 0; i < cids.length; i++){
      if (!cards[cids[i]] && !ignore){
        fetchData();
        break;
      }
    }

    return () => {
      ignore = true;
    }
  }, []);

  useEffect(() => {
    if (cards) searchCard(false);
  }, [cards, cardSortType, cids]);

  function sortCards(cardsToSort, sortType){
    if (sortType === "nameAscending"){
      cardsToSort.sort((a,b) => a.title.localeCompare(b.title)); 
    } else if (sortType === "nameDescending"){
      cardsToSort.sort((a,b) => b.title.localeCompare(a.title)); 
    } else if (sortType === "dateAscending"){
      cardsToSort.sort((a,b) => a.doc - b.doc); 
    } else if (sortType === "dateDescending"){
      cardsToSort.sort((a,b) => b.doc - a.doc); 
    } else if (sortType === "orderAscending"){
      cardsToSort.sort((a,b) => a.order - b.order); 
    } else if (sortType === "orderDescending"){
      cardsToSort.sort((a,b) => b.order - a.order); 
    }

    return cardsToSort;
  }

  function searchCard(nothing){
    let filteredSearch = search.trim();
    if (nothing){
      filteredSearch = "";
    }

    setSearch(filteredSearch);
    const re = new RegExp(`${filteredSearch}`, 'i');
    setCurrentCards(sortCards(deckCards.filter(card => re.test(card.title)), cardSortType));
  }

  function closeCardDialog(){
    setSearching(false);
    setState(null);
  }

  function closeFailedAuthorizationDialog(){
    setState(null);
    setAuth(false);
  }

  function deleteCard(card){
    setState("loading");
    fetch("https://quickcards-api.vercel.app/card", {
      method: "DELETE",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({cid: card.cid, did})
    })
    .then(res => {
      if (res.ok){
        setDialogBox("check", "Operation successful", <Text><Text style={qcs.b}>{card.title}</Text> was deleted successfully</Text>, null, closeCardDialog, closeCardDialog);

        let newCids;
        for(let i = 0; i < decks.length; i++){
          if (decks[i].did === did){
            const newDecks = decks;
            const index = newDecks[i].cards.indexOf(card.cid);
            if (index > -1) newDecks[i].cards.splice(index, 1);
            newCids = [...newDecks[i].cards];
            setDecks(newDecks);
            break;
          }
        }

        const newCards= cards;
        delete newCards[card.cid];

        newCids.forEach((cid, i) => {
          newCards[cid].order = i + 1;
        })  

        setCards(newCards);

        navigation.setParams({...route.params, cids: newCids});
      } else if(res.status === 403){
        setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);
      }
    })
    .catch(() => {
      setDialogBox("alert", "Something went wrong!", null, null, () => {setState(null)},() => {setState(null)});
    })
  }

  async function onRefresh(){
    setRefreshing(true);

    try{
      const deckRes = await fetch(`https://quickcards-api.vercel.app/deck/${did}`, {
        method: "GET",
        credentials: "include"
      });
      if (deckRes.status === 403) return setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);

      const jsonDeck = await deckRes.json();

      //update deck
      for(let i = 0; i < decks.length; i++){
        if (decks[i].did === did){
          const newDecks = [...decks];
          newDecks[i] = jsonDeck;
          setDecks(newDecks);
          break;
        }
      }

      const cardsRes = await fetch(`https://quickcards-api.vercel.app/cards/${did}`, {
        method: "GET",
        credentials: "include"
      })
      if (cardsRes.status === 403) return setAuth(false); 

      const jsonCards = await cardsRes.json();
      const cardDict = {};
      jsonCards.forEach(d => {
        d.doc = new Date(d.doc);
        d.order = jsonDeck.cards.indexOf(d.cid) + 1;

        cardDict[d.cid] = d;
      });

      //remove missing cards
      var missingCards = [];
      for(let i = 0; i < cids.length; i++){
        if (jsonDeck.cards.includes(cids[i]) === -1){
          missingCards.push(cids[i]);
        }
      }

      const newCards = {...cards};
      missingCards.forEach(cid => {
        Reflect.deleteProperty(newCards, cid);
      })

      navigation.setParams({
        cids: jsonDeck.cards
      });
      setCards({...newCards, ...cardDict});  
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
      searchCard(false);
    }

    setRefreshing(false);
  }

  return (
    <View style={qcs.container}>
      <Appbar.Header>
        {isSearching ?
          <View style={[qcs.w100, qcs.px2]}>
            <Searchbar
              onChangeText={text => setSearch(text)}
              value={search}
              placeholder="Search decks"
              traileringIcon="close"
              onSubmitEditing={() => {
                setSearching(false);
                searchCard(false);
              }}
              onIconPress={() => {
                setSearching(false);
                searchCard(false);
              }}
              onTraileringIconPress ={() => {
                setSearching(false);
                searchCard(true);
              }}
            /> 
          </View>:
          <>
            <Appbar.BackAction onPress={() => navigation.navigate('Home')} />
            <Appbar.Content title="Home"/>
            <Appbar.Action icon="magnify" onPress={() => setSearching(true)}/>
            <Appbar.Action icon="card-plus" onPress={() => navigation.navigate("CreateCard", route.params)}/>
            <Appbar.Action icon="dots-vertical" onPress={() => navigation.navigate("SortCard")}/>
          </>
        }

      </Appbar.Header>

      <View style={[qcs.px2, qcs.flex]}>
        {search && <Chip style={qcs.mt2} onPress={() => console.log('Pressed')} color={Colors[theme]}>Search result for "{search}"</Chip>}

        <View style={[qcs.flexRow, qcs.jcBetween, qcs.aiCenter, qcs.py1]}>
          <Text variant='titleLarge' style={[{color: Colors[theme]}, qcs.b]}>{name}</Text>
          <View style={[qcs.flexRow, qcs.aiCenter]}>
            <Text variant='titleLarge' style={[{color: Colors[theme]}, qcs.b]}>{cids.length}</Text>
            <Icon source="cards-outline" size={30} color={Colors[theme]}/>
          </View>
        </View>

        {/* <View style={[qcs.w100, qcs.border, qcs.borderTop, qcs.mb2]}/> */}


        {currentCards && currentCards.length > 0 ? 
          <FlatList
            data={currentCards}
            renderItem={({item, index}) => 
            <TouchableOpacity 
              onPress={() => navigation.navigate("ViewCard", {...route.params, cardsViewInfo, index})}
              onLongPress={() => setDialogBox("card-multiple-outline", `Card ${item.title}`, 
              <CardDialogContent {...item} navigation={navigation} setState={setState} setDialogBox={setDialogBox} closeCardDialog={closeCardDialog} 
                deleteCard={() => deleteCard(item)}
                routeParams={route.params}
              />, 
              null, closeCardDialog, closeCardDialog)}
            >
              <Card title={item.title} order={item.order} theme={theme} style={qcs.mb3}/>
            </TouchableOpacity>}
            keyExtractor={item => item.cid}
            contentContainerStyle={[qcs.aiCenter, {minHeight: 100}]}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          />
        :
          !state && <View style={[qcs.flex, qcs.center]}>
            <IconButton
              icon="emoticon-sad"
              size={60}
              iconColor={Colors[theme]}
            />
          </View>
        }
      </View>

    </View>
  )
}

const CardDialogContent = ({cid, title, note, order, doc, routeParams, navigation, setState, setDialogBox, closeCardDialog, deleteCard}) => {
  return(
    <View>
      <Text variant="titleMedium" style={[qcs.mb3]}>Made at {doc.toDateString()}</Text>
      <Button icon="file-edit" mode="outlined" onPress={() => {
        navigation.navigate('EditCard', {...routeParams, cid, title, order, note});
        setState(null);
      }} style={[qcs.mb2]} contentStyle={qcs.jcStart}>
        Edit card
      </Button>
      <Button icon="delete" mode="contained" onPress={() => {
        setDialogBox("delete-alert", "Deleting deck", 
        <Text>Are you sure you want to delete <Text style={qcs.b}>{title}</Text>?</Text>, 
        closeCardDialog,
        () => {deleteCard({cid, title})},
        closeCardDialog);
        setState("dialogAlt");
      }} style={[qcs.mb1]} contentStyle={qcs.jcStart}>
        Delete card
      </Button>
    </View>
  );
}


export default Cards;