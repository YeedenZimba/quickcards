import React, { useState, useContext } from 'react';
import { Text, TextInput, Button, Appbar } from 'react-native-paper';
import { View } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';

import Password from '../components/password';

import StateContext from '../contexts/state';
import UserContext from '../contexts/user';

const EditEmail = ({ navigation }) => {
  const [ invalidFeedback, setInvalidFeedback ] = useState(null);
  
  const { setState, setDialogBox } = useContext(StateContext);
  const { user, setUser } = useContext(UserContext);
  const [data, setData] = useState({email: user.email, password: ""});
  const [authPrompt, setAuthPrompt] = useState(false);


  function closeDialog(){
    setState(null);
  }

  function closeSuccessDialog(){
    setState(null);
    navigation.goBack();
  }

  async function onReview(){
    const filteredData = {email: data.email.trim(), password: data.password.trim()}
    setData(filteredData);
    setState("loading");
    setInvalidFeedback({input: "", feedback: ""});

    try{
      const res = await fetch(`https://quickcards-api.vercel.app/email-validate-only`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(filteredData)
      });

      if (res.ok){
        setState(null);
        setAuthPrompt(true);       
      }else{
        setState(null);
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);
          }
        }); 
      }
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
    }
  }

  async function onUpdate(){
    const filteredData = {email: data.email.trim(), password: data.password.trim()}
    setData(filteredData);
    setState("loading");
    setInvalidFeedback({input: "", feedback: ""});

    try{
      const res = await fetch("https://quickcards-api.vercel.app/email-change-request", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({...user, newEmail: data.email, password: data.password})
      });

      if(res.ok){
        setDialogBox("check", "Operation successful", <Text>An email has been sent to <Text style={qcs.b}>{data.email}</Text> for verification purposes</Text>, null, closeSuccessDialog, closeSuccessDialog);
      }
      else if(res.status === 401){
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);
            setState(null);
          }
        });
      }
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, closeDialog, closeDialog);
    }     
  }

  return (
    <View style={qcs.container}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Settings"/>
      </Appbar.Header>

      {!authPrompt ?
        <View style={[qcs.form, qcs.flex]}>
          <Text variant="displaySmall" style={[qcs.mt3, qcs.mb2]}>Change email</Text>

          <View>
            <View style={qcs.mb3}>
              <TextInput
                label="Email"
                mode="outlined"
                value={data.email}
                onChangeText={text => setData({...data, email: text})}
                error={invalidFeedback && invalidFeedback.input === "email"}
              />
              {invalidFeedback && invalidFeedback.input === "email" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
            </View>

            <Button icon="file-check" mode="contained" style={qcs.mt3} onPress={onReview}>
              Review change
            </Button>
          </View>
        </View> :
        <View style={[qcs.form, qcs.flex]}>
          <Text variant="displaySmall" style={qcs.mt3}>Authentication required</Text>
          <Text variant="titleMedium">An email will be sent to <Text style={qcs.b}>{data.email}</Text></Text>
          <View>
            <View style={qcs.mb3}>
              <Password 
                label="Password"
                value={data.password}
                onChangeText={text => setData({...data, password: text})}
                style={qcs.mt2}
                error={invalidFeedback && invalidFeedback.input === "password"}
              />
              {invalidFeedback && invalidFeedback.input === "password" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
            </View>

            <Button icon="file-check" mode="outlined" style={[qcs.mt3, qcs.mb2]} onPress={() => {setAuthPrompt(false)}}>
              Cancel
            </Button>
            <Button icon="file-check" mode="contained" onPress={onUpdate}>
              Update
            </Button>
          </View>
        </View>
      }
    </View>
  )
}


export default EditEmail;