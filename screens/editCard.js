import React, { useState, useContext, useRef } from 'react';
import { Text, TextInput, Button, Appbar } from 'react-native-paper';
import { View, ScrollView } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { SelectList } from 'react-native-dropdown-select-list'
import Slider from "react-native-a11y-slider";

import StateContext from '../contexts/state';
import DeckContext from '../contexts/deck';
import CardContext from '../contexts/card';

const EditCard = ({ route, navigation }) => {
  const {title, note, order, cids, did, cid} = route.params;
  const [data, setData] = useState({title, note, order});
  const [ invalidFeedback, setInvalidFeedback ] = useState(null);
  const [orderType, setOrderType] = useState(cids.length > 1 ? "between" : "first");
  const { setAuth, setState, setDialogBox } = useContext(StateContext);
  const { decks, setDecks } = useContext(DeckContext);
  const { cards, setCards } = useContext(CardContext);
  const maxOrder = cids.length + 1;
  const newCids = useRef(cids);

  const orders = cids.length > 1 ? [
    {key:'first', value:'First'},
    {key:'last', value:'Last'},
    {key:'between', value:'Between'}
  ] :
  [
    {key:'first', value:'First'},
    {key:'last', value:'Last'}
  ]

  function closeFailedAuthorizationDialog(){
    setState(null);
    setAuth(false);
  }

  function closeSuccessDialog(){
    setState(null);
    navigation.navigate("Cards", {...route.params, cids: newCids.current});
  }

  function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing
  };

  async function onEdit(){
    const filteredData = {...data, title: data.title.trim(), note: data.note.trim()}
    if (!filteredData.order) filteredData.order = 1;
    setData(filteredData);
    setState("loading");
    setInvalidFeedback({input: "", feedback: ""});

    try{
      const res = await fetch("https://quickcards-api.vercel.app/card", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify({...filteredData, cid: parseInt(cid), did: did})
      });

      if(!res.ok){
        if (res.status === 403){
          setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);
        } else if(res.status === 400){
          res.json().then(json => {
            if (json.input && json.feedback){
              setInvalidFeedback(json);
              setState(null);
            }
          });
        } else{
          throw new Error("Cannot update card");
        }
      }
      else{
        res.json().then(json => {
          const newCards = cards;
          newCards[json.cid]= json;
          newCards[json.cid].doc = new Date(json.doc);
        
          for(let i = 0; i < decks.length; i++){
            if (decks[i].did === did){
              const newDecks = decks;
              if (filteredData.order === -1){
                array_move(newDecks[i].cards, newDecks[i].cards.indexOf(json.cid), newDecks[i].cards.length - 1);
              }else{
                array_move(newDecks[i].cards, newDecks[i].cards.indexOf(json.cid), filteredData.order - 1);
              }
              newCids.current = [...newDecks[i].cards];
              setDecks(newDecks);
              break;
            }
          }

          newCids.current.forEach((cid, i) => {
            newCards[cid].order = i + 1;
          })          
          setCards(newCards);
        });
        setDialogBox("check", "Operation successful", <Text><Text style={qcs.b}>{data.title}</Text> has been updated successfully</Text>, null, closeSuccessDialog, closeSuccessDialog);
      }
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
    } 
  }

  return (
    <View style={qcs.container}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Back"/>
      </Appbar.Header>
      <ScrollView style={[qcs.form, qcs.flex]}>
        <Text variant="displaySmall" style={qcs.mt3}>Edit card</Text>
        <Text variant="titleMedium" style={qcs.mb3}>and study again</Text>

        <View>
          <View style={qcs.mb1}>
            <TextInput
              label="Title"
              mode="outlined"
              value={data.title}
              onChangeText={text => setData({...data, title: text})}
              error={invalidFeedback && invalidFeedback.input === "title"}
            />
            {invalidFeedback && invalidFeedback.input === "title" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
          </View>

          <View style={qcs.mb2}>
            <TextInput
              label="Note"
              mode="outlined"
              value={data.note}
              onChangeText={text => setData({...data, note: text})}
              error={invalidFeedback && invalidFeedback.input === "note"}
              multiline
              placeholder="Your note in here"
              numberOfLines={6}
            />
            {invalidFeedback && invalidFeedback.input === "note" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
          </View>

          <Text>Order</Text>
          <SelectList 
            setSelected={(value) => {
              setOrderType(value);
              if (value === "last"){
                setData({...data, order: -1});
              } else if(value === "first"){
                setData({...data, order: 1});
              } else{
                setData({...data, order});
              }
            }}
            search={false}
            data={orders} 
            save="key"
            value={orderType}
            boxStyles={[qcs.mb2, {backgroundColor: 'white'}]}
            dropdownStyles={{backgroundColor: 'white'}}
            defaultOption={{key: cids.length > 1 ? "between" : "first", value: cids.length > 1 ? "Between" : "First"}}
          />

          {orderType === "between" &&
            <View>
              <Text style={qcs.mb2}>Select card position</Text>
              <Slider
                min={1} 
                max={maxOrder} 
                values={[data.order]} 
                showLabel={false}
                onChange={n => setData({...data, order: n})}
                style={qcs.mb1}
              />
              <TextInput
                label="Position"
                mode="outlined"
                value={data.order.toString()}
                onChangeText={text =>{
                  text = text.trim();
                  text = text.replace(/[^0-9]/g, '');
                  if (text){
                    let n = parseInt(text);
                    n = Math.min(maxOrder, n);
                    n = Math.max(1, n);
                    setData({...data, order: n});
                  }else{
                    setData({...data, order: ""});
                  }
                }}
                error={invalidFeedback && invalidFeedback.input === "order"}
              />
            </View>
          }

          <Button icon="credit-card-edit-outline" mode="contained" style={qcs.mt3} onPress={onEdit}>
            EDIT CARD
          </Button>
        </View>
      </ScrollView>
    </View>
  )
}


export default EditCard;