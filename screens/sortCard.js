import React, { useState, useContext } from 'react';
import { Text, Button, Appbar, RadioButton } from 'react-native-paper';
import { View } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { SelectList } from 'react-native-dropdown-select-list';

import CardContext from '../contexts/card';

const SortCard = ({ navigation }) => {
  const sortTypes = [
    {key:'nameAscending', value:'Name ascending'},
    {key:'dateAscending', value:'Date ascending'},
    {key:'orderAscending', value:'Order ascending'},
    {key:'nameDescending', value:'Name descending'},
    {key:'dateDescending', value:'Date descending'},
    {key:'orderDescending', value:'Order descending'}
  ]

  const { cardSortType, setCardSortType } = useContext(CardContext);
  const [ dialogCardSortType, setDialogCardSortType ] = useState(cardSortType);
  
  let defaultValue;
  sortTypes.forEach(type => {
    if (type.key === cardSortType) defaultValue = type.value;
  })
  const defaultSortType = {key: cardSortType, value: defaultValue};

  return(
    <View style={qcs.container}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Cards"/>
      </Appbar.Header>
      <View style={[qcs.form, qcs.flex]}>
        <Text variant="displaySmall" style={qcs.mt3}>Sort cards</Text>
        <Text variant="titleMedium" style={qcs.mb3}>and find your cards faster</Text>
        
        {/* <RadioButton.Group 
          onValueChange={(value) => {
            setDialogcardSortType(value);
          }}
          value={dialogcardSortType}
        >
          <RadioButton.Item label="Name ascending" value="nameAscending" />
          <RadioButton.Item label="Date ascending" value="dateAscending" />
          <RadioButton.Item label="Cards ascending" value="cardsAscending" />
          <RadioButton.Item label="Name descending" value="nameDescending" />
          <RadioButton.Item label="Date descending" value="dateDescending" />
          <RadioButton.Item label="Cards descending" value="cardsDescending" />
        </RadioButton.Group> */}

        <SelectList 
            setSelected={setDialogCardSortType}
            search={false}
            data={sortTypes} 
            save="key"
            value={dialogCardSortType}
            boxStyles={[qcs.mb3, {backgroundColor: 'white'}]}
            dropdownStyles={{backgroundColor: 'white'}}
            defaultOption={defaultSortType}
          />

        <Button icon="sort" mode="contained" 
          onPress={() => {
            setCardSortType(dialogCardSortType);
            navigation.goBack();
          }}
          style={qcs.mt2}
        >
          Sort
        </Button>

      </View>
    </View>    
  );
}


export default SortCard;