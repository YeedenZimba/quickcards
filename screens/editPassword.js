import React, { useState, useContext } from 'react';
import { Text, TextInput, Button, Appbar } from 'react-native-paper';
import { View } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';

import Password from '../components/password';

import StateContext from '../contexts/state';
import UserContext from '../contexts/user';

const EditPassword = ({ navigation }) => {
  const [ invalidFeedback, setInvalidFeedback ] = useState(null);
  
  const { setState, setDialogBox } = useContext(StateContext);
  const { user, setUser } = useContext(UserContext);
  const [data, setData] = useState({password: "", newPassword: "", confirmPassword: ""});
  const [authPrompt, setAuthPrompt] = useState(false);

  function closeDialog(){
    setState(null);
  }

  function closeSuccessDialog(){
    setState(null);
    navigation.goBack();
  }

  async function onReview(){
    const filteredData = {...data, newPassword: data.newPassword.trim(), confirmPassword: data.confirmPassword.trim()}
    setData(filteredData);
    setState("loading");
    setInvalidFeedback({input: "", feedback: ""});

    try{
      const res = await fetch(`https://quickcards-api.vercel.app/password-validate-only`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({password: filteredData.newPassword, confirmPassword: filteredData.confirmPassword})
      });

      if (res.ok){
        setState(null);
        setAuthPrompt(true);       
      }else{
        setState(null);
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);
          }
        }); 
      }
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
    }
  }

  async function onUpdate(){
    const filteredData = {...data, password: data.password.trim()}
    setData(filteredData);
    setState("loading");
    setInvalidFeedback({input: "", feedback: ""});

    try{
      const res = await fetch(`https://quickcards-api.vercel.app/password`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({...user, password: filteredData.password, newPassword: filteredData.newPassword})
      });

      if (res.ok){
        setDialogBox("check", "Operation successful", <Text>Your password has been changed</Text>, null, closeSuccessDialog, closeSuccessDialog);
      }
      else if(res.status === 401){
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);
            setState(null);
          }
        });
      }
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, closeDialog, closeDialog);
    }    
  }

  return (
    <View style={qcs.container}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Settings"/>
      </Appbar.Header>

      {!authPrompt ?
        <View style={[qcs.form, qcs.flex]}>
          <Text variant="displaySmall" style={[qcs.mt3, qcs.mb1]}>Change password</Text>

          <View>
            <View style={qcs.mb1}>
              <Password 
                label="New password"
                value={data.newPassword}
                onChangeText={text => setData({...data, newPassword: text})}
                style={qcs.mt2}
                error={invalidFeedback && invalidFeedback.input === "newPassword"}
              />
              {invalidFeedback && invalidFeedback.input === "newPassword" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
            </View>

            <View style={qcs.mb3}>
              <Password 
                label="Confirm password"
                value={data.confirmPassword}
                onChangeText={text => setData({...data, confirmPassword: text})}
                style={qcs.mt2}
                error={invalidFeedback && invalidFeedback.input === "confirmPassword"}
              />
              {invalidFeedback && invalidFeedback.input === "confirmPassword" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
            </View>


            <Button icon="file-check" mode="contained" style={qcs.mt3} onPress={onReview}>
              Review change
            </Button>
          </View>
        </View> :
        <View style={[qcs.form, qcs.flex]}>
          <Text variant="displaySmall" style={qcs.mt3}>Authentication required</Text>
          <Text variant="titleMedium">Your password will be changed</Text>
          <View>
            <View style={qcs.mb3}>
              <Password 
                label="Password"
                value={data.password}
                onChangeText={text => setData({...data, password: text})}
                style={qcs.mt2}
                error={invalidFeedback && invalidFeedback.input === "password"}
              />
              {invalidFeedback && invalidFeedback.input === "password" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
            </View>

            <Button icon="file-check" mode="outlined" style={[qcs.mt3, qcs.mb2]} onPress={() => {setAuthPrompt(false)}}>
              Cancel
            </Button>
            <Button icon="file-check" mode="contained" onPress={onUpdate}>
              Update
            </Button>
          </View>
        </View>
      }
    </View>
  )
}


export default EditPassword;