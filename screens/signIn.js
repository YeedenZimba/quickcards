import React, { useState, useContext } from 'react';
import { Text, TextInput, Button } from 'react-native-paper';
import { View, Image } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';

import Password from '../components/password';

import StateContext from '../contexts/state';

const SignIn = ({ navigation }) => {
  const { setAuth, setState, setDialogBox } = useContext(StateContext);
  const [data, setData] = useState({email: "", password: ""});
  const [ invalidFeedback, setInvalidFeedback ] = useState(null);

  async function onSignIn(){
    const filteredData = {
      email: data.email.trim(),
      password: data.password.trim()
    }
    setInvalidFeedback(null);
    setData(filteredData);
    setState("loading");

    try{
      const res = await fetch("https://quickcards-api.vercel.app/sign-in", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify(filteredData)
      });
      if (res.ok){
        setAuth(true);
        setState(null);
      }else if(res.status === 400 || res.status === 401){
        setState(null);
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);
          }
        });
      }else{
        console.log(res);
      }
    }
    catch (err){
      setDialogBox("alert", "Login failed!", <Text>{err.message}. Please check your internet connection or try again later</Text>, null, () => {setState(null)}, () => {setState(null)});
    }
  }

  return (
    <View style={[qcs.container, qcs.center]}>
      <View style={qcs.form}>
        <View style={[qcs.center, qcs.mb2]}>
          <Image
            style={{width: 100, height: 100}}
            source={require('../assets/quickcards_logo.png')}
          />
        </View>
        <Text variant="displaySmall" style={qcs.textCenter}>Let's login</Text>
        <Text variant="titleMedium" style={[qcs.mb3, qcs.textCenter]}>And card to your memory</Text>

        <TextInput
          label="Email"
          mode="outlined"
          value={data.email}
          onChangeText={text => setData({...data, email: text})}
          error={invalidFeedback && invalidFeedback.input === "email"}
        />
        {invalidFeedback && invalidFeedback.input === "email" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}

        <Password 
          label="Password"
          value={data.password}
          onChangeText={text => setData({...data, password: text})}
          style={qcs.mt2}
          error={invalidFeedback && invalidFeedback.input === "password"}
        />
        {invalidFeedback && invalidFeedback.input === "password" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}

        <View style={[qcs.mb1, qcs.mt1, qcs.asStart]}>
          <Button mode="text" onPress={() => navigation.navigate("ForgotPassword")}>
            Forgot password?
          </Button>
        </View>

        <Button icon="login" mode="contained" onPress={onSignIn} style={[qcs.mb3]}>
          Login
        </Button>
        <Text style={[qcs.textCenter, qcs.mb2]}>OR</Text>
        <View>
          <Button mode="text" onPress={() => navigation.navigate('SignUp')}>
            Don't have an account? Register here
          </Button>
        </View>
      </View>
    </View>
  )
}


export default SignIn