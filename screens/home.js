import React, { useState, useEffect, useContext, useRef } from 'react';
import { View, TouchableOpacity, FlatList, RefreshControl } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { Searchbar, IconButton, RadioButton, Button, Chip, Text, Appbar } from 'react-native-paper';

import StateContext from '../contexts/state';
import DeckContext from '../contexts/deck';
import Deck from '../components/deck';

const iconSize = 30;

const Home = ({ navigation }) => {
  const [ isSearching, setSearching ] = useState(false);
  const [ refreshing, setRefreshing ] = useState(false);
  const [search, setSearch] = useState("");
  
  const { state, setAuth, setState, setDialogBox } = useContext(StateContext);
  const { decks, setDecks, deckSortType, setDeckSortType } = useContext(DeckContext);
  const [ currentDecks, setCurrentDecks ] = useState(decks);
  
  useEffect(() => {
    let ignore = false;

    const fetchData = async () => {
      setState("loading");
      try{
        const decksRes = await fetch("https://quickcards-api.vercel.app/decks", {
          method: "GET",
          credentials: "include"
        })
        if (decksRes.status === 403) return setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);; 

        const jsonDecks = await decksRes.json();
        jsonDecks.forEach(d => {
          d.doc = new Date(d.doc);
        });
  
        if (ignore) return;
  
        setDecks(jsonDecks);
        setState(null);
      }
      catch (err){
        setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
      }

    }

    fetchData();

    return () => {
      ignore = true;
    }
  }, []);

  useEffect(() => {
    if (decks) searchDeck(false);
  }, [decks, deckSortType]);

  function sortDecks(decksToSort, sortType){
    if (sortType === "nameAscending"){
      decksToSort.sort((a,b) => a.name.localeCompare(b.name)); 
    } else if (sortType === "nameDescending"){
      decksToSort.sort((a,b) => b.name.localeCompare(a.name)); 
    } else if (sortType === "dateAscending"){
      decksToSort.sort((a,b) => a.doc - b.doc); 
    } else if (sortType === "dateDescending"){
      decksToSort.sort((a,b) => b.doc - a.doc); 
    } else if (sortType === "cardsAscending"){
      decksToSort.sort((a,b) => a.cards.length - b.cards.length); 
    } else if (sortType === "cardsDescending"){
      decksToSort.sort((a,b) => b.cards.length - a.cards.length); 
    }

    return decksToSort;
  }

  function searchDeck(nothing){
    let filteredSearch = search.trim();
    if (nothing){
      filteredSearch = "";
    }

    setSearch(filteredSearch);
    const re = new RegExp(`${filteredSearch}`, 'i');
    setCurrentDecks(sortDecks(decks.filter(deck => re.test(deck.name)), deckSortType));
  }

  function closeDeckDialog(){
    setState(null);
  }

  function closeFailedAuthorizationDialog(){
    setState(null);
    setAuth(false);
  }

  function deleteDeck(deck){
    setState("loading");
    fetch("https://quickcards-api.vercel.app/deck", {
      method: "DELETE",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({did: deck.did})
    })
    .then(res => {
      if (res.ok){
        setDialogBox("check", "Operation successful", <Text><Text style={qcs.b}>{deck.name}</Text> was deleted successfully</Text>, null, closeDeckDialog, closeDeckDialog);
        const newDecks = decks.filter(d => d.did !== deck.did);
        setDecks(newDecks);
      } else if(res.status === 403){
        setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);
      }
    })
    .catch(() => {
      setDialogBox("alert", "Something went wrong!", null, null, () => {setState(null)},() => {setState(null)});
    })
  }

  async function onRefresh(){
    setRefreshing(true);

    try{
      const decksRes = await fetch("https://quickcards-api.vercel.app/decks", {
        method: "GET",
        credentials: "include"
      })
      if (decksRes.status === 403) return setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);; 

      const jsonDecks = await decksRes.json();
      jsonDecks.forEach(d => {
        d.doc = new Date(d.doc);
      });

      setDecks(jsonDecks);
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
      setCurrentDecks(sortDecks(currentDecks, deckSortType));
    }

    setRefreshing(false);
  }

  return (
    <View style={qcs.container}>
      <Appbar.Header>
        <View style={[qcs.w100, qcs.px2]}>
          {!isSearching &&
            <View style={[qcs.flexRow, qcs.jcBetween]}>
              <IconButton
                icon="magnify"
                size={iconSize}
                onPress={() => {
                  setSearching(true);
                }}
              />
              <IconButton
                icon="plus-box"
                size={iconSize}
                onPress={() => navigation.navigate("CreateDeck")}
              />
              <IconButton
                icon="dots-vertical"
                size={iconSize}
                onPress={() => {
                  navigation.navigate("SortDeck");
                }}
              />
            </View>
          }

          {isSearching &&
            <Searchbar
              onChangeText={text => setSearch(text)}
              value={search}
              placeholder="Search decks"
              traileringIcon="close"
              onSubmitEditing={() => {
                setSearching(false);
                searchDeck(false);
              }}
              onIconPress={() => {
                setSearching(false);
                searchDeck(false);
              }}
              onTraileringIconPress ={() => {
                setSearching(false);
                searchDeck(true);
              }}
            />
          }

        </View>
      </Appbar.Header>
      <View style={[qcs.px2, qcs.flex]}>
        {search && !isSearching && <Chip style={qcs.mt2} onPress={() => console.log('Pressed')}>Search result for "{search}"</Chip>}

        {currentDecks && currentDecks.length > 0 ?
          <FlatList
            data={currentDecks}
            renderItem={({item}) => 
            <TouchableOpacity 
              onPress={() => navigation.navigate("DeckNav", {did: item.did, name: item.name, cids: item.cards, theme: item.theme})}
              onLongPress={() => setDialogBox("card-multiple-outline", `Deck ${item.name}`, 
              <DeckDialogContent {...item} navigation={navigation} setState={setState} setDialogBox={setDialogBox} closeDeckDialog={closeDeckDialog} deleteDeck={deleteDeck}/>, 
              null, closeDeckDialog, closeDeckDialog)}
            >
              <Deck name={item.name} cards={item.cards.length} theme={item.theme} style={qcs.mt2}/>
            </TouchableOpacity>}
            contentContainerStyle={{minHeight: 100}}
            keyExtractor={item => item.did}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          />
          :
          !state && <View style={[qcs.flex, qcs.center]}>
            <IconButton
              icon="emoticon-sad"
              size={60}
            />
          </View>
        }
      </View>  
    </View>
  )
}

const DeckDialogContent = ({did, name, cards, doc, theme, navigation, setState, setDialogBox, closeDeckDialog, deleteDeck}) => {
  return(
    <View>
      <Text variant="titleMedium" style={[qcs.mb1]}>Made at {doc.toDateString()}</Text>
      <Text variant="titleMedium" style={[qcs.mb1]}>{cards.length} {cards.length > 1 ? "cards" : "card"} inside</Text>
      <Text variant="titleMedium" style={[qcs.mb3]}>Theme {theme}</Text>
      <Button icon="file-edit" mode="outlined" onPress={() => {
        navigation.navigate('EditDeck', {deck :{did, name, theme}});
        setState(null);
      }} style={[qcs.mb2]} contentStyle={qcs.jcStart}>
        Edit deck
      </Button>
      <Button icon="delete" mode="contained" onPress={() => {
        setDialogBox("delete-alert", "Deleting deck", 
        <Text>Are you sure you want to delete <Text style={qcs.b}>{name}</Text> along with the {cards.length} {cards.length > 1 ? "cards" : "card"} inside it?</Text>, 
        closeDeckDialog,
        () => {deleteDeck({did, name})},
        closeDeckDialog);
        setState("dialogAlt");
      }} style={[qcs.mb1]} contentStyle={qcs.jcStart}>
        Delete deck
      </Button>
    </View>
  );
}



export default Home;