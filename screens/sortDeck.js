import React, { useState, useContext } from 'react';
import { Text, Button, Appbar, RadioButton } from 'react-native-paper';
import { View } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { SelectList } from 'react-native-dropdown-select-list';

import DeckContext from '../contexts/deck';

const SortDeck = ({ navigation }) => {
  const sortTypes = [
    {key:'nameAscending', value:'Name ascending'},
    {key:'dateAscending', value:'Date ascending'},
    {key:'cardsAscending', value:'Cards ascending'},
    {key:'nameDescending', value:'Name descending'},
    {key:'dateDescending', value:'Date descending'},
    {key:'cardsDescending', value:'Cards descending'}
  ]

  const { deckSortType, setDeckSortType } = useContext(DeckContext);
  const [ dialogDeckSortType, setDialogDeckSortType ] = useState(deckSortType);
  
  let defaultValue;
  sortTypes.forEach(type => {
    if (type.key === deckSortType) defaultValue = type.value;
  })
  const defaultSortType = {key: deckSortType, value: defaultValue};

  return(
    <View style={qcs.container}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.navigate('Home')} />
        <Appbar.Content title="Home"/>
      </Appbar.Header>
      <View style={[qcs.form, qcs.flex]}>
        <Text variant="displaySmall" style={qcs.mt3}>Sort decks</Text>
        <Text variant="titleMedium" style={qcs.mb3}>and find your decks faster</Text>
        
        {/* <RadioButton.Group 
          onValueChange={(value) => {
            setDialogDeckSortType(value);
          }}
          value={dialogDeckSortType}
        >
          <RadioButton.Item label="Name ascending" value="nameAscending" />
          <RadioButton.Item label="Date ascending" value="dateAscending" />
          <RadioButton.Item label="Cards ascending" value="cardsAscending" />
          <RadioButton.Item label="Name descending" value="nameDescending" />
          <RadioButton.Item label="Date descending" value="dateDescending" />
          <RadioButton.Item label="Cards descending" value="cardsDescending" />
        </RadioButton.Group> */}

        <SelectList 
            setSelected={setDialogDeckSortType}
            search={false}
            data={sortTypes} 
            save="key"
            value={dialogDeckSortType}
            boxStyles={[qcs.mb3, {backgroundColor: 'white'}]}
            dropdownStyles={{backgroundColor: 'white'}}
            defaultOption={defaultSortType}
          />

        <Button icon="sort" mode="contained" 
          onPress={() => {
            setDeckSortType(dialogDeckSortType);
            navigation.goBack();
          }}
          style={qcs.mt2}
        >
          Sort
        </Button>

      </View>
    </View>    
  );
}


export default SortDeck;