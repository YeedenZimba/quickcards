import React, { useState, useContext } from 'react';
import { Text, TextInput, Button, Appbar } from 'react-native-paper';
import { View } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';

import Password from '../components/password';

import StateContext from '../contexts/state';

const ForgotPassword = ({ navigation }) => {
  const { setState, setDialogBox } = useContext(StateContext);
  const [data, setData] = useState({email: "", password: "", confirmPassword: ""});
  const [ invalidFeedback, setInvalidFeedback ] = useState(null);

  async function onNext(){
    const filteredData = {
      email: data.email.trim(),
      password: data.password.trim(),
      confirmPassword: data.confirmPassword.trim() 
    }

    setData(filteredData);
    setInvalidFeedback({input: "", feedback: ""});
    setState("loading");

    try{
      const res = await fetch("https://quickcards-api.vercel.app/forgot-password-request", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(filteredData)
      });

      if(res.ok){
        setDialogBox("email-fast-outline", "Reset password request successful", <Text>Email sent to <Text style={qcs.b}>{data.email}</Text>. Please check your inbox and note that the mail may take a while to get in there</Text>, null, closeSuccessDialog, closeSuccessDialog);
        setInvalidFeedback({input: "", feedback: ""});
      }else if(res.status === 400){
        res.json().then(json => {
          if (json.input && json.feedback){
            setState(null);
            setInvalidFeedback(json);
          }
        });
      }
    }
    catch (err){
      setDialogBox("alert", "Failed to sent a password request!", <Text>{err.message}. Please check your internet connection or try again later</Text>, null, () => {setState(null)}, () => {setState(null)});
    }
  }

  function closeSuccessDialog(){
    setState(null);
    navigation.goBack();
  }

  return (
    <View style={qcs.container}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Back"/>
      </Appbar.Header>
      <View style={[qcs.form, qcs.flex]}>
        <Text variant="displaySmall" style={qcs.mt3}>Reset password</Text>
        <Text variant="titleMedium" style={qcs.mb3}>and try not to forget!</Text>

        <View style={[qcs.flex, qcs.jcBetween, {maxHeight: 360}]}>
          <View>
            <TextInput
              label="Email Address"
              mode="outlined"
              value={data.email}
              onChangeText={text => setData({...data, email: text})}
              error={invalidFeedback && invalidFeedback.input === "email"}
            />
            {invalidFeedback && invalidFeedback.input === "email" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
          </View>

          <View>
            <Password 
              label="Password"
              value={data.password}
              onChangeText={text => setData({...data, password: text})}
              error={invalidFeedback && invalidFeedback.input === "password"}
            />
            {invalidFeedback && invalidFeedback.input === "password" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
          </View>

          <View>
            <Password 
                label="Retype Password"
                value={data.confirmPassword}
                onChangeText={text => setData({...data, confirmPassword: text})}
                error={invalidFeedback && invalidFeedback.input === "confirmPassword"}
              />
              {invalidFeedback && invalidFeedback.input === "confirmPassword" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
          </View>

          <Button icon="lock-reset" mode="contained" onPress={onNext}>
            Next
          </Button>
        </View>
      </View>
    </View>
  )
}


export default ForgotPassword;