import React, { useState, useEffect, useContext } from 'react';
import { View, TouchableOpacity, ScrollView, RefreshControl} from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { Avatar, Text} from 'react-native-paper';
import Icon from 'react-native-paper/src/components/Icon';

import StatusSpace from '../components/statusSpace';

import StateContext from '../contexts/state';
import UserContext from '../contexts/user';
import DeckContext from '../contexts/deck';

const Settings = ({ navigation }) => {
  const [ refreshing, setRefreshing ] = useState(false);
  const { setAuth, setState, setDialogBox } = useContext(StateContext);
  const { user, setUser } = useContext(UserContext);
  const { setDecks } = useContext(DeckContext);

  useEffect(() => {
    let ignore = false;

    if (!user){
      setState("loading");
      fetch("https://quickcards-api.vercel.app/user", {
        method: "GET",
        credentials: "include"
      })
      .then(res => {
        setState(null);

        if (res.ok && !ignore){
          res.json()
          .then(json => {
            const date = new Date(json.doc);
            setUser({...json, doc: date});
          })
        } else if(res.status === 403 && !ignore){
          setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);
        }
      })
      .catch(err => {
        setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
      })
    }
    
    return () => {ignore = true}
  }, []);

  async function onRefresh(){
    setRefreshing(true);

    fetch("https://quickcards-api.vercel.app/user", {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      setRefreshing(false);
      if (res.ok){
        res.json()
        .then(json => {
          const date = new Date(json.doc);
          setUser({...json, doc: date});
        })
      } else if(res.status === 403){
        setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);
      }
    })
    .catch(err => {
      setRefreshing(false);
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
    })
  }

  async function signOut(){
    setState("loading");

    try{
      const res = await fetch("https://quickcards-api.vercel.app/sign-out", {
        method: "DELETE",
        credentials: "include"
      });

      if (res.ok){
        setAuth(false);
        setUser(null);
        setDecks(null);
      }
    }
    catch(err){
      console.log(err.message);
    }

    setState(null);
  }

  function closeFailedAuthorizationDialog(){
    setState(null);
    setAuth(false);
  }

  return (
    <ScrollView 
      contentContainerStyle={[qcs.container, qcs.px2]}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      <StatusSpace/>

      <View style={[qcs.flexRow, qcs.w100, qcs.mt3]}>
        <Avatar.Text size={70} label={user ? user.name.charAt(0) : ""} style={qcs.me1}/>
        <View style={qcs.jcCenter}>
          <Text variant="titleLarge" style={qcs.mb1}>{user && user.name}</Text>
          <View style={[qcs.flexRow, qcs.aiCenter]}>
            <Icon source="email-outline" size={18}/>
            <Text variant="bodyMedium" style={qcs.ms1}>{user && user.email}</Text>
          </View>
        </View>
      </View>
      
      <View style={[qcs.w100, qcs.border, qcs.borderTop, qcs.mt2, qcs.mb2]}/>

      <Text variant="bodyMedium" style={[qcs.asStart, qcs.mb2]}>APP SETTINGS</Text>

      <TouchableOpacity style={[qcs.flexRow, qcs.aiCenter, qcs.py2]} onPress={() => {navigation.navigate('EditName')}}>
        <Icon source="account-edit-outline" size={30}/>
        <Text variant="titleMedium" style={[qcs.ms2, qcs.flex]}>Edit username</Text>
        <Icon source="chevron-right" size={30}/>
      </TouchableOpacity>

      <TouchableOpacity style={[qcs.flexRow, qcs.aiCenter, qcs.py2]} onPress={() => {navigation.navigate('EditEmail')}}>
        <Icon source="email-edit-outline" size={30}/>
        <Text variant="titleMedium" style={[qcs.ms2, qcs.flex]}>Change email</Text>
        <Icon source="chevron-right" size={30}/>
      </TouchableOpacity>

      <TouchableOpacity style={[qcs.flexRow, qcs.aiCenter, qcs.py2]} onPress={() => {navigation.navigate('EditPassword')}}>
        <Icon source="lock-outline" size={30}/>
        <Text variant="titleMedium" style={[qcs.ms2, qcs.flex]}>Change password</Text>
        <Icon source="chevron-right" size={30}/>
      </TouchableOpacity>
      
      <View style={[qcs.w100, qcs.border, qcs.borderTop, qcs.mt2, qcs.mb2]}/>

      <TouchableOpacity style={[qcs.flexRow, qcs.asStart, qcs.py2]} onPress={signOut}>
        <Icon source="logout" size={30} color="red"/>
        <Text variant="titleMedium" style={[qcs.ms2, {color: 'red'}]}>Logout</Text>
      </TouchableOpacity>

    </ScrollView>
  )
}


export default Settings