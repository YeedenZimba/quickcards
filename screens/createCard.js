import React, { useState, useContext, useRef } from 'react';
import { Text, TextInput, Button, Appbar } from 'react-native-paper';
import { View, ScrollView } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { SelectList } from 'react-native-dropdown-select-list'
import Slider from "react-native-a11y-slider";

import StateContext from '../contexts/state';
import DeckContext from '../contexts/deck';
import CardContext from '../contexts/card';

const CreateCard = ({ route, navigation }) => {
  const {cids, did} = route.params;
  const [data, setData] = useState({title: "", note: "", order: 1});
  const [ invalidFeedback, setInvalidFeedback ] = useState(null);
  const [orderType, setOrderType] = useState("first");
  const { setAuth, setState, setDialogBox } = useContext(StateContext);
  const { decks, setDecks } = useContext(DeckContext);
  const { cards, setCards } = useContext(CardContext);
  const maxOrder = cids.length + 2;
  const newCids = useRef(cids);

  const orders = cids.length > 0 ? [
    {key:'first', value:'First'},
    {key:'last', value:'Last'},
    {key:'between', value:'Between'}
  ] :
  [
    {key:'first', value:'First'},
    {key:'last', value:'Last'}
  ]

  function closeFailedAuthorizationDialog(){
    setState(null);
    setAuth(false);
  }

  function closeSuccessDialog(){
    setState(null);
    navigation.navigate("Cards", {...route.params, cids: newCids.current});
  }

  async function onCreate(){
    const filteredData = {...data, title: data.title.trim(), note: data.note.trim()}
    if (!filteredData.order) filteredData.order = 1;
    setData(filteredData);
    setState("loading");
    setInvalidFeedback({input: "", feedback: ""});

    try{
      const res = await fetch("https://quickcards-api.vercel.app/card", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify({...filteredData, did})
      });

      if(!res.ok){
        if (res.status === 403){
          setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);
        } else if(res.status === 400){
          res.json().then(json => {
            if (json.input && json.feedback){
              setInvalidFeedback(json);
              setState(null);
            }
          });
        }
      }
      else{
        res.json().then(json => {
          const newCards = cards;
          newCards[json.cid]= json;
          newCards[json.cid].doc = new Date(json.doc);
        
          for(let i = 0; i < decks.length; i++){
            if (decks[i].did === did){
              const newDecks = decks;
              if (filteredData.order === -1){
                newDecks[i].cards.push(json.cid);
              }else{
                newDecks[i].cards.splice(filteredData.order - 1, 0, json.cid);
              }
              newCids.current = [...newDecks[i].cards];
              setDecks(newDecks);
              break;
            }
          }

          newCids.current.forEach((cid, i) => {
            newCards[cid].order = i + 1;
          })          
          setCards(newCards);
        });
        setDialogBox("check", "Operation successful", <Text><Text style={qcs.b}>{data.title}</Text> was created successfully</Text>, null, closeSuccessDialog, closeSuccessDialog);
      }
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
    } 
  }

  return (
    <View style={qcs.container}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Back"/>
      </Appbar.Header>
      <ScrollView style={[qcs.form, qcs.flex]}>
        <Text variant="displaySmall" style={qcs.mt3}>Create card</Text>
        <Text variant="titleMedium" style={qcs.mb3}>and start learning</Text>

        <View>
          <View style={qcs.mb1}>
            <TextInput
              label="Title"
              mode="outlined"
              value={data.title}
              onChangeText={text => setData({...data, title: text})}
              error={invalidFeedback && invalidFeedback.input === "title"}
            />
            {invalidFeedback && invalidFeedback.input === "title" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
          </View>

          <View style={qcs.mb2}>
            <TextInput
              label="Note"
              mode="outlined"
              value={data.note}
              onChangeText={text => setData({...data, note: text})}
              error={invalidFeedback && invalidFeedback.input === "note"}
              multiline
              placeholder="Your note in here"
              numberOfLines={6}
            />
            {invalidFeedback && invalidFeedback.input === "note" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
          </View>

          <Text>Order</Text>
          <SelectList 
            setSelected={(value) => {
              setOrderType(value);
              if (value === "last"){
                setData({...data, order: -1});
              } else{
                setData({...data, order: 1});
              }
            }}
            search={false}
            data={orders} 
            save="key"
            value={orderType}
            boxStyles={[qcs.mb2, {backgroundColor: 'white'}]}
            dropdownStyles={{backgroundColor: 'white'}}
            defaultOption={{key:'first', value:'First'}}
          />

          {orderType === "between" &&
            <View>
              <Text style={qcs.mb2}>Select card position</Text>
              <Slider
                min={1} 
                max={maxOrder} 
                values={[data.order]} 
                showLabel={false}
                onChange={n => setData({...data, order: n})}
                style={qcs.mb1}
              />
              <TextInput
                label="Position"
                mode="outlined"
                value={data.order.toString()}
                onChangeText={text =>{
                  text = text.trim();
                  text = text.replace(/[^0-9]/g, '');
                  if (text){
                    let n = parseInt(text);
                    n = Math.min(maxOrder, n);
                    n = Math.max(1, n);
                    setData({...data, order: n});
                  }else{
                    setData({...data, order: ""});
                  }
                }}
                error={invalidFeedback && invalidFeedback.input === "order"}
              />
            </View>
          }

          <Button icon="card-plus-outline" mode="contained" style={qcs.mt3} onPress={onCreate}>
            CREATE CARD
          </Button>
        </View>
      </ScrollView>
    </View>
  )
}


export default CreateCard;