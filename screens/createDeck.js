import React, { useState, useContext } from 'react';
import { Text, TextInput, Button, Appbar } from 'react-native-paper';
import { View } from 'react-native';
import { quickcardsStyles as qcs } from '../styles';
import { SelectList } from 'react-native-dropdown-select-list'

import StateContext from '../contexts/state';
import DeckContext from '../contexts/deck';

const CreateDeck = ({ navigation }) => {
  const [data, setData] = useState({deckName: "", theme: "default"});
  const [ invalidFeedback, setInvalidFeedback ] = useState(null);

  const { setAuth, setState, setDialogBox } = useContext(StateContext);
  const { decks, setDecks } = useContext(DeckContext);

  const themes = [
    {key:'default', value:'Default'},
    {key:'salmon', value:'Salmon'},
    {key:'sky', value:'Sky'},
    {key:'lucky', value:'Lucky'},
    {key:'banana', value:'Banana'},
    {key:'sunset', value:'Sunset'}
  ]

  function closeFailedAuthorizationDialog(){
    setState(null);
    setAuth(false);
  }

  function closeSuccessDialog(){
    setState(null);
    navigation.navigate("Home");
  }

  async function onCreate(){
    const filteredData = {...data, deckName: data.deckName.trim()}
    setData(filteredData);
    setState("loading");
    setInvalidFeedback({input: "", feedback: ""});

    try{
      const res = await fetch("https://quickcards-api.vercel.app/deck", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify(filteredData)
      });

      if(!res.ok){
        if (res.status === 403){
          setDialogBox("alert", "Failed to authorize user", null, null, closeFailedAuthorizationDialog, closeFailedAuthorizationDialog);
        } else if(res.status === 400){
          res.json().then(json => {
            if (json.input && json.feedback){
              setInvalidFeedback(json);
              setState(null);
            }
          });
        }
      }
      else{
        setDialogBox("check", "Operation successful", <Text><Text style={qcs.b}>{data.deckName}</Text> was created successfully</Text>, null, closeSuccessDialog, closeSuccessDialog);
        res.json().then(json => {
          setDecks([...decks, {...json, doc: new Date(json.doc)}]);
        });
      }
    }
    catch (err){
      setDialogBox("alert", "Something went wrong!", <Text>{err.message}</Text>, null, () => {setState(null)},() => {setState(null)});
    }
  }

  return (
    <View style={qcs.container}>
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.navigate('Home')} />
        <Appbar.Content title="Home"/>
      </Appbar.Header>
      <View style={[qcs.form, qcs.flex]}>
        <Text variant="displaySmall" style={qcs.mt3}>Create deck</Text>
        <Text variant="titleMedium" style={qcs.mb3}>and organize your learning</Text>

        <View>
          <View style={qcs.mb3}>
            <TextInput
              label="Name"
              mode="outlined"
              value={data.deckName}
              onChangeText={text => setData({...data, deckName: text})}
              error={invalidFeedback && invalidFeedback.input === "deckName"}
            />
            {invalidFeedback && invalidFeedback.input === "deckName" && <Text variant="bodyMedium" style={{color: 'red'}}>{invalidFeedback.feedback}</Text>}
          </View>

          <Text>Theme</Text>
          <SelectList 
            setSelected={(value) => setData({...data, theme: value})}
            search={false}
            data={themes} 
            save="key"
            value={data.theme}
            boxStyles={[qcs.mb3, {backgroundColor: 'white'}]}
            dropdownStyles={{backgroundColor: 'white'}}
            defaultOption={{key:'default', value:'Default'}}
          />

          <Button icon="folder-plus" mode="contained" style={qcs.mt3} onPress={onCreate}>
            CREATE DECK
          </Button>
        </View>
      </View>
    </View>
  )
}


export default CreateDeck;