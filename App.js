import React, { useState, useEffect, useRef } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Provider as PaperProvider } from 'react-native-paper';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import Loading from './components/loading';
import DialogBox from './components/dialogBox';
import SignIn from './screens/signIn';
import SignUp from './screens/signUp';
import ForgotPassword from './screens/forgotPassword';
import TabNavigator from './navigators/tabNavigator';
import ViewCard from './screens/viewCard';

import StateContext from './contexts/state';
import UserContext from './contexts/user';
import DeckContext from './contexts/deck';
import CardContext from './contexts/card';


const Stack = createNativeStackNavigator();

export default function App() {
  const [ state, setState ] = useState("loading");
  const [isAuth, setAuth] = useState(false);
  const [user, setUser] = useState(null);

  const [decks, setDecks] = useState(null);
  const [deckSortType, setDeckSortType] = useState("nameAscending");

  const [cards, setCards] = useState({});
  const [cardSortType, setCardSortType] = useState("orderAscending");

  const dialogBox = useRef(null);

  function setDialogBox(icon, title, content, onCancel, onOkay, onDismiss){
    dialogBox.current= {icon, title, content, onCancel, onOkay, onDismiss};
    setState("dialog");
  }

  useEffect(() => {
    let ignore = false;
    fetch("https://quickcards-api.vercel.app/is-verify", {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      !ignore && setState(null);
      res.ok && !ignore && setAuth(true);
    })
    .catch(() => {
      setDialogBox("alert", "User verification failed!", null, null, () => {setState(null)}, () => {setState(null)});
    })
    return () => {ignore = true}
  }, []);

  return (
    <StateContext.Provider value={{state, setAuth, setState, setDialogBox}}>
      <UserContext.Provider value={{user, setUser}}>
        <DeckContext.Provider value={{decks, setDecks, deckSortType, setDeckSortType}}>
          <CardContext.Provider value={{cards, setCards, cardSortType, setCardSortType}}>
              <PaperProvider theme={theme}>
                <Loading loading={state === "loading"}/>

                <DialogBox 
                  visible={state === "dialog" || state === "dialogAlt"} 
                  onDismiss={dialogBox.current?.onDismiss}
                  icon={dialogBox.current?.icon}
                  title={dialogBox.current?.title}
                  content={dialogBox.current?.content}
                  onCancel={dialogBox.current?.onCancel}
                  onOkay={dialogBox.current?.onOkay}
                />
                <SafeAreaProvider>
                  <NavigationContainer>
                    <Stack.Navigator initialRouteName={!isAuth? "SignIn" : "TabNav"} screenOptions={{
                      headerShown: false
                    }}>
                      {!isAuth ?
                        <>
                          <Stack.Screen name="SignIn" component={SignIn} />
                          <Stack.Screen name="SignUp" component={SignUp} />
                          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
                        </> : 
                        <>
                          <Stack.Screen name="TabNav" component={TabNavigator} />
                          <Stack.Screen name="ViewCard" component={ViewCard} />
                        </>
                      }

                    </Stack.Navigator>
                  </NavigationContainer>
                </SafeAreaProvider>
              </PaperProvider>
          </CardContext.Provider>
        </DeckContext.Provider>
      </UserContext.Provider>
    </StateContext.Provider>
  );
}

const theme = {
  "colors": {
    "primary": "rgb(151, 77, 231)",//
    "onPrimary": "rgb(255, 255, 255)",
    "primaryContainer": "rgb(240, 219, 255)",
    "onPrimaryContainer": "rgb(44, 0, 81)",
    "secondary": "rgb(102, 90, 111)",
    "onSecondary": "rgb(255, 255, 255)",
    "secondaryContainer": "rgb(151, 77, 231)",//
    "onSecondaryContainer": "rgb(255, 255, 255)",//
    "tertiary": "rgb(128, 81, 88)",
    "onTertiary": "rgb(255, 255, 255)",
    "tertiaryContainer": "rgb(255, 217, 221)",
    "onTertiaryContainer": "rgb(50, 16, 23)",
    "error": "red",//
    "onError": "rgb(255, 255, 255)",
    "errorContainer": "rgb(255, 218, 214)",
    "onErrorContainer": "rgb(65, 0, 2)",
    "background": "rgb(255, 251, 255)",
    "onBackground": "rgb(29, 27, 30)",
    "surface": "rgb(255, 251, 255)",
    "onSurface": "rgb(29, 27, 30)",
    "surfaceVariant": "rgb(233, 223, 235)",
    "onSurfaceVariant": "rgb(74, 69, 78)",
    "outline": "rgb(124, 117, 126)",
    "outlineVariant": "rgb(204, 196, 206)",
    "shadow": "rgb(0, 0, 0)",
    "scrim": "rgb(0, 0, 0)",
    "inverseSurface": "rgb(50, 47, 51)",
    "inverseOnSurface": "rgb(245, 239, 244)",
    "inversePrimary": "rgb(220, 184, 255)",
    "elevation": {
      "level0": "transparent",
      "level1": "rgb(248, 242, 251)",
      "level2": "rgb(244, 236, 248)",
      "level3": "rgb(240, 231, 246)",
      "level4": "rgb(239, 229, 245)",
      "level5": "rgb(236, 226, 243)"
    },
    "surfaceDisabled": "rgba(29, 27, 30, 0.12)",
    "onSurfaceDisabled": "rgba(29, 27, 30, 0.38)",
    "backdrop": "rgba(51, 47, 55, 0.4)"
  }
};
