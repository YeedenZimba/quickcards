import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from '../screens/home';
import CreateDeck from '../screens/createDeck';
import EditDeck from '../screens/editDeck';
import SortDeck from '../screens/sortDeck';
import DeckNavigator from './deckNavigator';

const Stack = createNativeStackNavigator();

export default function HomeNavigator() {
  return (
    <Stack.Navigator initialRouteName="Home" screenOptions={{
      headerShown: false
    }}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="CreateDeck" component={CreateDeck} />
      <Stack.Screen name="EditDeck" component={EditDeck} />
      <Stack.Screen name="SortDeck" component={SortDeck} />
      <Stack.Screen name="DeckNav" component={DeckNavigator} />
    </Stack.Navigator>
  );
}
