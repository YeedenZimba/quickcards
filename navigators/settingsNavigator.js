import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Settings from '../screens/settings';
import EditName from '../screens/editName';
import EditEmail from '../screens/editEmail';
import EditPassword from '../screens/editPassword';

const Stack = createNativeStackNavigator();

export default function SettingsNavigator() {
  return (
    <Stack.Navigator initialRouteName="Settings" screenOptions={{
      headerShown: false
    }}>
      <Stack.Screen name="Settings" component={Settings} />
      <Stack.Screen name="EditName" component={EditName} />
      <Stack.Screen name="EditEmail" component={EditEmail} />
      <Stack.Screen name="EditPassword" component={EditPassword} />
    </Stack.Navigator>
  );
}
