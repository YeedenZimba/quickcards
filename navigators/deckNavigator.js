import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Cards from '../screens/cards';
import CreateCard from '../screens/createCard';
import EditCard from '../screens/editCard';
import SortCard from '../screens/sortCard';

const Stack = createNativeStackNavigator();

export default function DeckNavigator({route}) {
  return (
    <Stack.Navigator initialRouteName="Cards" screenOptions={{
      headerShown: false
    }}>
      <Stack.Screen name="Cards" component={Cards} initialParams={route.params}/>
      <Stack.Screen name="CreateCard" component={CreateCard} initialParams={route.params}/>
      <Stack.Screen name="EditCard" component={EditCard}/>
      <Stack.Screen name="SortCard" component={SortCard}/>
    </Stack.Navigator>
  );
}
