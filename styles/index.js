import { StyleSheet } from 'react-native';
import { spacingStyles } from './spacing';
import { formStyles } from './form';
import { boxStyles } from './box';
import { typographyStyles } from './typography';
import { flexStyles } from './flex';

export const quickcardsStyles = StyleSheet.create({
  ...spacingStyles,
  ...formStyles,
  ...boxStyles,
  ...typographyStyles,
  ...flexStyles
});