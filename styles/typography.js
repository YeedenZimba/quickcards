import { StyleSheet } from 'react-native';

export const typographyStyles = StyleSheet.create({
  textCenter: {
    textAlign: 'center'
  },
  textLeft : {
    textAlign: 'left'
  },
  b: {
    fontWeight: 'bold'
  },
  u: {
    textDecorationLine: 'underline',
  },
  default: {
    color: 'black'
  },
  salmon: {
    color: 'white'
  },
  sky: {
    color: 'white'
  },
  lucky: {
    color: 'white'
  },
  banana: {
    color: 'white'
  },
  sunset: {
    color: 'white'
  }
});