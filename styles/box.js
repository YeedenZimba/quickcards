import { StyleSheet } from 'react-native';

export const boxStyles = StyleSheet.create({
  border: {
    borderWidth: 1,
    borderColor: 'rgb(160, 160, 160)'
  },
  borderTop: {
    borderWidth: 0,
    borderTopWidth: 1,
  },
  container: {
    width: '100%',
    flex: 1
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  hCenter: {
    display: 'flex',
    alignItems: 'center'
  },
  vCenter: {
    display: 'flex',
    justifyContent: 'center'
  },
  w100: {
    width: '100%'
  },
  h100: {
    height: '100%'
  },
  dNone: {
    display: 'none'
  },
  pAbsolute: {
    position: 'absolute'
  }
});