import { StyleSheet } from 'react-native';

export const spacingStyles = StyleSheet.create({
  //top
  mt1: {
    marginTop: 10
  },
  mt2: {
    marginTop: 20
  },
  mt3: {
    marginTop: 30
  },
  //bottom
  mb1: {
    marginBottom: 10
  },
  mb2: {
    marginBottom: 20
  },
  mb3: {
    marginBottom: 30
  },
  //start or left
  ms1: {
    marginLeft: 10
  },
  ms2: {
    marginLeft: 20
  },
  ms3: {
    marginLeft: 30
  },
  //end or right
  me1: {
    marginRight: 10
  },
  me2: {
    marginRight: 20
  },
  me3: {
    marginRight: 30
  },

  //padding
  p1: {
    padding: 10
  },
  p2: {
    padding: 20
  },  
  p3: {
    padding: 30
  },
  //x axis
  px1: {
    paddingLeft: 10,
    paddingRight: 10
  },
  px2: {
    paddingLeft: 20,
    paddingRight: 20
  },  
  px3: {
    paddingLeft: 30,
    paddingRight: 30
  },
  //y axis
  py1: {
    paddingTop: 10,
    paddingBottom: 10
  },
  py2: {
    paddingTop: 20,
    paddingBottom: 20
  },  
  py3: {
    paddingTop: 30,
    paddingBottom: 30
  }
});