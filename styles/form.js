import { StyleSheet } from 'react-native';

export const formStyles = StyleSheet.create({
  form: {
    width: '100%',
    maxWidth: 500,
    paddingLeft: 20,
    paddingRight: 20
  }
});