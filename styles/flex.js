import { StyleSheet } from 'react-native';

export const flexStyles = StyleSheet.create({
  dFlex: {
    display: 'flex'
  },
  flexRow: {
    flexDirection: 'row'
  },
  flexColumn: {
    flexDirection: 'column'
  },
  flexColumnReverse: {
    flexDirection: 'column-reverse'
  },
  jcStart: {
    justifyContent: 'flex-start'
  },
  jcCenter: {
    justifyContent: 'center'
  },
  jcBetween: {
    justifyContent: 'space-between' 
  },
  jcAround : {
    justifyContent: 'space-around'
  },
  aiCenter: {
    alignItems: 'center'
  },
  aiStart: {
    alignItems: 'flex-start'
  },
  aiStrech: {
    alignItems: 'stretch'
  },
  asCenter: {
    alignSelf: 'center'
  },
  asStart: {
    alignSelf: 'flex-start'
  },
  asStrech: {
    alignSelf: 'stretch'
  },  
  flex: {
    flex: 1
  }
});