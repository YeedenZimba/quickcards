import React from "react";
import { Image } from "react-native";
import { quickcardsStyles as qcs } from "../styles";

const Logo = () => {
  <Image
    style={[{width: 50, height: 50}, qcs.border]}
    source={require('../assets/quickcards_logo.png')}
  />
}

export default Logo;