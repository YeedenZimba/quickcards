import { View, StatusBar } from "react-native";

const StatusSpace = () => {
  return (
    <View style={{height: StatusBar.currentHeight}}/>
  );
}

export default StatusSpace;
