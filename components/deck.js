import React from "react";
import { View, StyleSheet } from "react-native";
import { Text } from "react-native-paper";
import { quickcardsStyles as qcs } from "../styles";


const Deck = ({name, cards, theme, style}) => {
  return (
    <View style={[styles.deck, qcs.jcCenter, styles[theme], style]}>
      <Text variant="titleLarge" style={[qcs.b, theme !== "default" && {color: 'white'}]}>{name}</Text>
      <Text variant="bodyMedium" style={[qcs.b, theme !== "default" && {color: 'white'}]}>{cards} {cards > 1 ? "cards" : "card"}</Text>
    </View>
  );
}


const styles = StyleSheet.create({
  deck: {
    height: 80,
    borderWidth: 1,
    borderRadius: 10,
    paddingLeft: 20
  },
  default: {
    borderColor: 'gray',
    backgroundColor: 'white'
  },
  salmon: {
    borderColor: '#De4c4c',
    backgroundColor: '#De4c4c'
  },
  sky: {
    borderColor: '#328ace',
    backgroundColor: '#328ace'
  },
  lucky: {
    borderColor: '#20b45e',
    backgroundColor: '#20b45e'  
  },
  banana: {
    borderColor: '#e2d000',
    backgroundColor: '#e2d000'
  },
  sunset: {
    borderColor: '#f2810f',
    backgroundColor: '#f2810f'
  }
});


export default Deck;