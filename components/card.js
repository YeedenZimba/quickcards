import React from "react";
import { View, StyleSheet } from "react-native";
import { Text } from "react-native-paper";
import { quickcardsStyles as qcs } from "../styles";


const Card = ({order, title, theme, style}) => {
  return (
    <View style={[styles.card, qcs.center, styles[theme], style]}>
      <Text variant="titleMedium" style={[qcs.pAbsolute, theme !== "default" && {color: 'white'}, {top: 10, left: 10}]}>{order}</Text>
      <Text variant="titleLarge" style={[qcs.b, theme !== "default" && {color: 'white'}]}>{title}</Text>
    </View>
  );
}


const styles = StyleSheet.create({
  card: {
    height: 130,
    width: 180,
    borderWidth: 1,
    borderRadius: 10
  },
  default: {
    borderColor: 'gray',
    backgroundColor: 'white'
  },
  salmon: {
    borderColor: '#De4c4c',
    backgroundColor: '#De4c4c'
  },
  sky: {
    borderColor: '#328ace',
    backgroundColor: '#328ace'
  },
  lucky: {
    borderColor: '#20b45e',
    backgroundColor: '#20b45e'  
  },
  banana: {
    borderColor: '#e2d000',
    backgroundColor: '#e2d000'
  },
  sunset: {
    borderColor: '#f2810f',
    backgroundColor: '#f2810f'
  }
});


export default Card;