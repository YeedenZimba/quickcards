import React from "react";
import { quickcardsStyles as qcs } from "../styles";
import { Portal, Dialog, Button } from 'react-native-paper';

const DialogBox = ({visible, icon, title, content, onCancel, onOkay, onDismiss}) => {
  return (
    <Portal>
      <Dialog visible={visible} onDismiss={onDismiss}>
        <Dialog.Icon icon={icon} />
        <Dialog.Title style={qcs.textCenter}>{title}</Dialog.Title>
        <Dialog.Content>
          {content}
        </Dialog.Content>
        <Dialog.Actions>
          {onCancel && <Button onPress={() => {
            onCancel();
          }}>
            Cancel
          </Button>}
          <Button onPress={() => {
            onOkay();
          }}>
            Okay
          </Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
}

export default DialogBox;