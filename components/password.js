import React, { useState } from "react";
import { TextInput } from 'react-native-paper';

const Password = ({label, value, onChangeText, style, error}) => {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <TextInput
      label={label}
      mode="outlined"
      value={value}
      secureTextEntry={!isVisible}
      onChangeText={onChangeText}
      right={<TextInput.Icon icon={isVisible ? "eye-off" : "eye"} onPress={(e) => {setIsVisible(!isVisible)}}/>}
      style={style}
      error={error}
    />
  );
}

export default Password;