import React from "react";
import { quickcardsStyles as qcs } from "../styles";
import { ActivityIndicator, Modal, Portal} from 'react-native-paper';

const Loading = ({loading}) => {
  return (
    <Portal>
      <Modal visible={loading} contentContainerStyle={qcs.container}>
        <ActivityIndicator size="large" animating={true}/>
      </Modal>
    </Portal>
  );
}

export default Loading;